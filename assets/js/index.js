let main = () => {
  let input_arr = [];
  let set_input = () => {
    let input_data =
      parseFloat(document.querySelectorAll(".inputNumber")[0].value) || 0;
    input_arr.push(input_data);
    document.querySelectorAll(
      ".txt-result-input"
    )[0].innerHTML += `${input_data} `;
  };
  let get_positive_item = (data_arr) => {
    return data_arr.filter((item) => item > 0);
  };
  let find_max_index = (data_arr) => {
    let max_index = 0;
    data_arr.forEach((item, idx) => {
      if (item > data_arr[max_index]) {
        max_index = idx;
      }
    });

    return max_index;
  };

  let find_min_index = (data_arr) => {
    let min_index = 0;
    data_arr.forEach((item, idx) => {
      if (item < data_arr[min_index]) {
        min_index = idx;
      }
    });

    return min_index;
  };
  let is_prime_number = (numb) => {
    let i,
      flag = 1;
    if (numb < 2) {
      return -1;
    }

    for (i = 2; i <= Math.sqrt(numb); i++) {
      if (numb % i === 0) {
        flag = 0;
        break;
      }
    }

    return flag;
  };
  let bai_1 = () => {
    let sum_positive = 0;
    sum_positive = get_positive_item(input_arr).reduce((total, curr) => {
      return total + curr;
    }, 0);
    document.querySelectorAll(
      ".txt-result-bai-1"
    )[0].innerHTML = `${sum_positive} `;
  };

  let bai_2 = () => {
    document.querySelectorAll(".txt-result-bai-2")[0].innerHTML = `Số dương: ${
      get_positive_item(input_arr).length
    } `;
  };

  let bai_3 = () => {
    let min_index = find_min_index(input_arr);
    document.querySelectorAll(
      ".txt-result-bai-3"
    )[0].innerHTML = `Số nhỏ nhất: ${input_arr[min_index]}`;
  };

  let bai_4 = () => {
    let result = "Không có số dương trong mảng";
    if (get_positive_item(input_arr).length) {
      let min_index = find_min_index(get_positive_item(input_arr));
      result = `Số dương nhỏ nhất: ${input_arr[min_index]}`;
    }
    document.querySelectorAll(".txt-result-bai-4")[0].innerHTML = result;
  };

  let bai_5 = () => {
    let curr_even_numb = 0;
    input_arr.forEach((item) => {
      !(item % 2) && (curr_even_numb = item);
    });
    document.querySelectorAll(
      ".txt-result-bai-5"
    )[0].innerHTML = `Số chẵn cuối cùng: ${curr_even_numb}`;
  };

  let bai_6 = () => {
    let temp;
    let index_1 = parseInt(document.querySelectorAll(".inputPos1")[0].value);
    let index_2 = parseInt(document.querySelectorAll(".inputPos2")[0].value);

    temp = input_arr[index_1];
    input_arr[index_1] = input_arr[index_2];
    input_arr[index_2] = temp;

    document.querySelectorAll(
      ".txt-result-bai-6"
    )[0].innerHTML = `Mảng sau khi đổi: ${input_arr}`;
  };

  let bai_7 = () => {
    input_arr.sort((a, b) => a - b);
    document.querySelectorAll(
      ".txt-result-bai-7"
    )[0].innerHTML = `Mảng sau khi sắp xếp: ${input_arr}`;
  };

  let bai_8 = () => {
    let i = 0,
      result = "Không có số nguyên tố";
    for (i = 0; i < input_arr.length; i++) {
      if (is_prime_number(input_arr[i]) === 1) {
        result = input_arr[i];
        break;
      }
    }
    document.querySelectorAll(".txt-result-bai-8")[0].innerHTML = `${result}`;
  };
  let bai_9 = (bai9Input) => {
    let count_integer = 0;
    bai9Input.forEach((item) => {
      if (Number.isInteger(item)) {
        count_integer++;
      }
    });

    document.querySelectorAll(
      ".txt-result-bai-9"
    )[0].innerHTML = `Số nguyên: ${count_integer}`;
  };
  let bai_10 = () => {
    let count_positive = 0,
      count_negative = 0,
      compare;

    count_positive = get_positive_item(input_arr).length;
    count_negative = input_arr.length - count_positive;
    if (count_negative < count_positive) {
      compare = "<";
    } else if (count_negative > count_positive) {
      compare = ">";
    } else {
      compare = "=";
    }
    document.querySelectorAll(
      ".txt-result-bai-10"
    )[0].innerHTML = `Số âm ${compare} Số dương`;
  };

  document.querySelectorAll(".btn-add")[0].addEventListener("click", set_input);
  document
    .querySelectorAll(".btn-calc-bai-1")[0]
    .addEventListener("click", bai_1);
  document
    .querySelectorAll(".btn-calc-bai-2")[0]
    .addEventListener("click", bai_2);

  document
    .querySelectorAll(".btn-calc-bai-3")[0]
    .addEventListener("click", bai_3);
  document
    .querySelectorAll(".btn-calc-bai-4")[0]
    .addEventListener("click", bai_4);

  document
    .querySelectorAll(".btn-calc-bai-5")[0]
    .addEventListener("click", bai_5);

  document
    .querySelectorAll(".btn-calc-bai-6")[0]
    .addEventListener("click", bai_6);

  document
    .querySelectorAll(".btn-calc-bai-7")[0]
    .addEventListener("click", bai_7);
  document
    .querySelectorAll(".btn-calc-bai-8")[0]
    .addEventListener("click", bai_8);

  let bai9Input = [];
  document
    .querySelectorAll(".bai-tap-9 .btn-add")[0]
    .addEventListener("click", () => {
      let input =
        parseFloat(document.querySelectorAll(".bai-tap-9 .inputN")[0].value) ||
        0;
      bai9Input.push(input);

      document.querySelectorAll(
        ".txt-input-bai-9"
      )[0].innerHTML = `${bai9Input.join(", ")}`;
    });
  document
    .querySelectorAll(".btn-calc-bai-9")[0]
    .addEventListener("click", () => {
      bai_9(bai9Input);
    });
  document
    .querySelectorAll(".btn-calc-bai-10")[0]
    .addEventListener("click", bai_10);
};

main();
